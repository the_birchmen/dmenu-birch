dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.


Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

```
make clean install
```

Running dmenu
-------------
See the man page for details.


Patches I have added 
------------ 
dmenu_run_i<br> dmenu-lineheight-4.9.diff<br> dmenu-border-4.9.diff<br>
dmenu-center-20200111-8cd37e1.diff<br>
dmenu-caseinsensitive-20200523-db6093f.diff<br>
dmenu-highlight-4.9.diff<br> dmenu-xresources-4.9.diff<br>
